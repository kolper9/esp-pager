panel_h=50;
panel_w=36.1;
panel_l=80.25;
wall_thickness=1.2;
pad_holder_h=15;
pad_holder_w=5.3;
pad_holder_l=5.3;
pad_hole_r=1.5;
$fn=90;
module back_panel(){
     // pcb pads  
  difference() {
  union(){ 
  translate([-3+(panel_h-pad_holder_h)/2,(panel_w-pad_holder_w)/2,(panel_l-pad_holder_l)/2]) cube([pad_holder_h,pad_holder_w,pad_holder_w],center=true) ;
   translate([-3+(panel_h-pad_holder_h)/2,-(panel_w-pad_holder_w)/2,(panel_l-pad_holder_l)/2]) cube([pad_holder_h,pad_holder_w,pad_holder_w],center=true) ;
   translate([-3+(panel_h-pad_holder_h)/2,-(panel_w-pad_holder_w)/2,-(panel_l-pad_holder_l)/2]) cube([pad_holder_h,pad_holder_w,pad_holder_w],center=true) ;
   translate([-3+(panel_h-pad_holder_h)/2,(panel_w-pad_holder_w)/2,-(panel_l-pad_holder_l)/2]) cube([pad_holder_h,pad_holder_w,pad_holder_w],center=true) ;
    
  }
    translate([2+(panel_h-pad_holder_h)/2,(panel_w-pad_holder_w)/2,(panel_l-pad_holder_l)/2]) rotate([0,90,0]) cylinder(pad_holder_h+1,pad_hole_r,pad_hole_r,center=true) ;
  translate([2+(panel_h-pad_holder_h)/2,-(panel_w-pad_holder_w)/2,(panel_l-pad_holder_l)/2]) rotate([0,90,0]) cylinder(pad_holder_h+1,pad_hole_r,pad_hole_r,center=true) ;
  translate([2+(panel_h-pad_holder_h)/2,-(panel_w-pad_holder_w)/2,-(panel_l-pad_holder_l)/2]) rotate([0,90,0]) cylinder(pad_holder_h+1,pad_hole_r,pad_hole_r,center=true) ;
  translate([2+(panel_h-pad_holder_h)/2,(panel_w-pad_holder_w)/2,-(panel_l-pad_holder_l)/2]) rotate([0,90,0]) cylinder(pad_holder_h+1,pad_hole_r,pad_hole_r,center=true) ;

  } 
   
 
 //traffic lights case 
    
  difference() {
  union(){ 
  
   translate([-(panel_h)/2+wall_thickness/2,0,0])  cube([wall_thickness,panel_w,panel_l],center=true) ; //back
    
   translate([0,(panel_w+wall_thickness)/2,0])  cube([panel_h,wall_thickness,panel_l],center=true) ; //left
    
      translate([0,-(panel_w+wall_thickness)/2,0])  cube([panel_h,wall_thickness,panel_l],center=true) ; //right
    
     translate([0,0,(panel_l+wall_thickness)/2])  cube([panel_h,panel_w+2*wall_thickness,wall_thickness],center=true) ; //top
    
    translate([0,0,-(panel_l+wall_thickness)/2])  cube([panel_h,panel_w+2*wall_thickness,wall_thickness],center=true) ; //bottom
    
  }
  
   translate([-2,0,-(panel_l+wall_thickness)/2]) cube([19.5,12.5,wall_thickness+1],center=true) ;
    translate([-(panel_h+wall_thickness)/2+7,0,+(panel_l+wall_thickness)/2]) cube([9,12,wall_thickness+1],center=true) ;
  
  }
   
  
    
    }
    
    
    back_panel();
    
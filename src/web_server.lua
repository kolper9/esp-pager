LENGTH_ROW=32;

text_pos=0;
sended_text="";
text="";
lcd_mod = require("lcd_module")
lcd_mod.init()
lcd_mod.cursor(0)
scroll_type=0;

local function printOnDisp(text) --function show text on display

    
   
    lcd_mod.cls();
    lcd_mod.home();
    text1=string.sub(text, 0,16)
    
    if #text<15 then  --one row
        lcd_mod.lcdprint(text1,1,0);
    else 
        text2=string.sub(text, 17,32)  --two rows
        lcd_mod.lcdprint(text1,1,0);
        lcd_mod.lcdprint(text2,2,0);       
   end
end





local function printNumber()  --function called by timer to show overflowed text
  lentext=string.len(text)
    if (lentext>=LENGTH_ROW )then

        if scroll_type == 0 then  --static
            if text_pos~=0 then
                text_pos=0
                printOnDisp(text)
            end   
            return
        elseif scroll_type == 1 then  -- jumping
            text_pos=(text_pos+LENGTH_ROW)
            if text_pos>lentext then
               text_pos=0
            end
            printOnDisp(string.sub (text, text_pos, text_pos+LENGTH_ROW))
        elseif scroll_type == 2 then   -- runing
            text_pos=(text_pos+1)%(lentext+1)
            printOnDisp(string.sub (text, text_pos, text_pos+LENGTH_ROW))
        elseif scroll_type == 3 then -- runing cyclic
            text_pos=(text_pos+1)%(lentext+LENGTH_ROW)
            text_con=string.sub (text, text_pos, text_pos+LENGTH_ROW)
            if(text_pos>lentext-LENGTH_ROW+1) then
            text_con=text_con..string.sub(text, 0, text_pos-lentext)
            end
            printOnDisp(text_con)
        end
        
    end  
end

local mytimer = tmr.create():alarm(2000, tmr.ALARM_AUTO,printNumber)


local function hex_to_char (x)
  return string.char(tonumber(x, 16))
end

local function urldecode (url) --decode text from url code
  if url == nil then
    return
  end
  url = url:gsub("+", " ")
  url = url:gsub("%%(%x%x)", hex_to_char)
  return url
end


printOnDisp("Offline pager   by Petr Kolar")
-- a simple HTTP server
srv = net.createServer(net.TCP)
tmr.delay(2000)
lcd_mod.cls();
srv:listen(80, function(conn)
    conn:on("receive", function(sck, request)
    
        print("Available heap1: "..node.heap())


        
        local buf = "";
        buf = buf.."HTTP/1.1 200 OK\n\n";
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end
        local _, _, auth = string.find(request, "%cAuthorization: Basic ([%w=\+\/]+)");--Authorization:
        
          if (auth == nil or auth ~= "YWRtaW46YWRtaW4=")then --user:pass
               Authenticate="HTTP/1.0 401 Authorization Required\r\nWWW-Authenticate: Basic realm=\"ESP8266 Web Server\"\r\n\r\n<h1>Unauthorized Access</h1>";
               sck:send(Authenticate);
               
               
               return;
          end
        local _GET = {}
        if (vars ~= nil) then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                _GET[k] = v
            end
        end


        

            
        fnd,fnd_end = string.find(request,"SendtoDisp=")
        if fnd ~= nil then
          text=string.sub(request, fnd)
          text=string.sub(text, (fnd_end-fnd)+2)
          text=urldecode(text)
          text = string.gsub(text, '[ \t]+%f[\r\n%z]', '') --remove trailing white space
          printOnDisp(text)
          print(text)
          sended_text=text.."<br />"..sended_text
        end 

        fnd,fnd_end = string.find(request,"DeleteHist=")
        if fnd ~= nil then
         sended_text="";
        end 

        fnd,fnd_end = string.find(request,"runRadio=")
        if fnd ~= nil then
         scroll_type=string.sub(request, fnd)
         scroll_type=string.sub(scroll_type, (fnd_end-fnd)+2)
         scroll_type=tonumber(scroll_type)
         print(scroll_type==1)
        end 
       
        
        



        buf = buf.."<head>";
        
        buf = buf.."<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">";
        buf = buf.."<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
        buf = buf.."<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>";
        buf = buf.."<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">";
        buf = buf.."</head><div class=\"container\">";


       
        
        buf = buf.."<div class=\"page-header\">";
        buf = buf.."    <h1>Online pager</h1>";
        buf = buf.."<p> Stránka pro zobrazení textu na displeji. \
        Pro napsání textu na displej napište text do formuláře a \
        klikněte na tačitko odeslat. Text bude zobrazen na dispeji \
        připojenému k ESP-01. Pod ním si můžete prohlédnou historii \
        odeslaných textů. </p> ";
        buf = buf.."</div>";
        
     

        
        buf = buf.."<form action=\"\" method=\"POST\">";
        buf = buf.."<div class=\"form-group\">";

        buf = buf.."<label class=\"form-control\" for=\"fname\"> Napište text k odeslání na zařízení: </label> ";
        buf = buf.."<input type=\"text\" id=\"toSend\" name=\"SendtoDisp\"><br><br>";
        buf = buf.."<input type=\"submit\"  class=\"btn btn-primary\" value=\"Odeslat\">";
        buf = buf.."</div>";
        buf = buf.."</form>";

        buf = buf.."<form action=\"\" method=\"POST\">";
      
        buf = buf.."<div class=\"page-header\">"
        buf = buf.."<h3 class=\"panel-title\">Výběr zobrazení přetékajícího textu</h3>";
        buf = buf.."</div>"
        buf = buf.."<div class=\"custom-control custom-radio\">"
        buf = buf.."<input type=\"radio\" id=\"dontRun\" value=\"0\" name=\"runRadio\" class=\"custom-control-input\" >"
        buf = buf.."<label class=\"custom-control-label\" for=\"customRadio1\">Statický text</label>"
        buf = buf.."</div>"
        buf = buf.."<div class=\"custom-control custom-radio\">"
        buf = buf.."<input type=\"radio\" id=\"jump\" value=\"1\" name=\"runRadio\" class=\"custom-control-input\">"
        buf = buf.."<label class=\"custom-control-label\" for=\"customRadio1\">Skákájící text</label>"
        buf = buf.."</div>"
        buf = buf.."<div class=\"custom-control custom-radio\">"
        buf = buf.."<input type=\"radio\" id=\"textRun\" value=\"2\" name=\"runRadio\" class=\"custom-control-input\">"
        buf = buf.."<label class=\"custom-control-label\" for=\"customRadio1\">Obíhající text</label>"
        buf = buf.."</div>"
        buf = buf.."<div class=\"custom-control custom-radio\">"
        buf = buf.."<input type=\"radio\" id=\"textRunCyclic\" value=\"3\" name=\"runRadio\" class=\"custom-control-input\">"
        buf = buf.."<label class=\"custom-control-label\" for=\"customRadio1\">Obíhající text cyklicky</label>"
        buf = buf.."</div>"
        buf = buf.."<input type=\"submit\" value=\"Zadat\" class=\"btn btn-success\">"
        buf = buf.."</form>";

    
        
        buf = buf.."<div class=\"panel panel-info\">";
        buf = buf.."    <div class=\"panel-heading\">";
        buf = buf.."      <h3 class=\"panel-title\">Historie odeslaných zpráv</h3>";
        buf = buf.."    </div>";
        buf = buf.."    <div class=\"panel-body\">";
        buf = buf.."      "..sended_text.."";

        buf = buf.."    </div>";
        buf = buf.."  </div>";

        buf = buf.."<form action=\"\" method=\"POST\">";
        buf = buf.."<div class=\"form-group\">";
        buf = buf.."<input type=\"submit\" name=\"DeleteHist\" class=\"btn btn-danger\" value=\"Odstranit historii\">";
        buf = buf.."</div>";
        buf = buf.."</form>";
        print("Available heap: "..node.heap())     
        
        sck:send(buf)
    end)
    conn:on("sent", function(sck) sck:close() end)
end)


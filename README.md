# Zobrazení textu na dálku PAGER
## Popis projektu 
Na server běžícím na zařízení se pošle zpráva, která je následně zobrazena na displeji připojeném k zařízení. Server běžící na zařízení je dostupný v lokální síti. Uživatel muže zadat text který se má zobrazit na displeji pomocí webového formuláře. Server zobrazuje informace, o již odeslaných textech a umožnuje výběr způsobu zobrazení přetékajícího textu. Webový server je zabezpečený pomocí jména a hesla. 
## Využívané technologie
Zařízení: ESP-01, dvouřádkový LCD displej s i2c převodníkem lcm1602, step-up boost convertor, step-down convertor, ftdi rs232 usb převodník

Software: NodeMCU

Programovací jazyk: Lua

## Seznam součástek
* 1S balancer s nabíjením pro lithiovou baterii
* 3.7 V lithiová baterie 18650
* 2x 10kΩ rezistor
* DC DC 5V step up
* DC DC adjustable step down
* ESP-01
* dvouřádkový LCD displej s i2c převodníkem lcm1602 
* ftdi rs232 USB převodník 
* kolébkový vypínač
 


## NodeMCU
Aplikace využívá firmware nodeMCU ve verzi 3.0.0. s moduly bit, file, gpio, i2c, net, node, tmr, uart, wifi. Pro nahrání nodeMCU byl využitý program Pyflasher. Verze, kterou jsem si stáhnul z gitu společnosti, vyžadovala doinstalování potřebných komponent a za komentování některých řádku kódu, aby šla vůbec spustit. Pro flashování firmwaru jsem nejprve využíval flash download tool. Po nahrání novějších verzí nodeMCU na zařízení ESP 01 se již neuměl se zařízením spojit.  
Webový server
Webový server nejprve provede kontrolu, zda mu bylo zaslané heslo pro přihlášení pomocí http basic authentication, pokud ne pak o něj zažádá. Pokud je autentizace úspěšná pokračuje zpracování požadavku. Parametry požadavku jsou předávány serveru pomocí metody POST. Server zpracovává 3 typy požadavků. Prvním je požadavek na zobrazení textu, další je požadavek na typ zobrazení přetékajícího textu a poslední je požadavek na smazání historie zaslaných textů. Požadavek na zobrazení textu probíhá následujícím zpracováním. Nejprve je zaslaný text nutné převést z URL kódu to textu (v URL kódu jsou nahrazené některé znaky). Následně může být text zobrazen na displeji. Poté je stránka v html uložená do proměnné buf a odeslána zpátky klientovi. 
Pro vylepšení vzhledu webové stránky byl použit bootstrap, který umožnuje vytvořit pěkné grafické prvky (jako jsou tlačítka, nápisy atd.), bez navýšení zátěže serveru a je velmi úsporný na velikost. Soubor css si prohlížeč stahuje se serveru bootstrapu, což je velké plus na embedded zařízeních.  

## Zobrazení přetékajícího se textu 
Způsoby zobrazení přetékajícího textu jsou popsány v dokumentaci. Zde popíši samotný mechanismus zobrazení. V aplikace je vytvořen časovač, který každé dvě vteřiny vyvolá přerušení a zavolá funkci pro přepsání textu. Tato funkce, pokud text přetéká, inkrementuje proměnou text_pos a překreslí zobrazovaný text na základě vybrané metody zobrazení textu.

## Zabezpečení
Webový server je zabezpečený pomocí http basic authentication. Nejedná se o dostatečný způsob zabezpečení heslo je posílání pouze zakódováno v base64 a to dokonce při každém připojení k serveru. Útočník má tedy velkou šanci heslo zachytit. Správným řešením by bylo použití https serveru. NodeMCU podporuje bohužel pouze https klienta. Našel jsem některé implementace https serveru na ESP01 jejich zprovoznění však nebylo jednoduché a výkon nebyl nijak oslnující. Heslo je uložené na zařízení pouze zakódované v base64. Řešení uložit pouze za hashované a osolené heslo. Tato operace vyžaduje modul crypto. Bohužel se mi již začínala docházet volná paměť na zařízení, a tak jsem tento modul zavrhnul.   
## Pouzdro zařízení
Zařízení je umístněné v pouzdře vytištěném na 3D tiskárně. Rozměry pouzdra jsou 39x50x84 mm. Materiál tisku je PLA. Pouzdro obsahuje dva otvory jeden pro napájení druhy pro kolébkový vypínač. Pro uchycení displeje jsou v pouzdru 4 držáky s dírami pro šrouby. Pouzdro bylo vymodelováno v programu openSCAD.  
## Hardwarové úpravy
Z důvodu rozdílného napájení displeje (5 V) a esp01 (3.3 V) bylo nutné odstranit z řadiče displeje pull up rezistory na i2c sběrnici. Tyto rezistory byly nahrazeny pull up rezistory na rozvodné desce, kde byly připojeny na napájení 3.3V. Tato rozvodná deska slouží jako propojovací rozhraní, jsou na ní přítomné dvě napájecí hladiny 5V a 3.3V. Dále je zde také zem. Ze step up boostru bylo nutné odebrat USB konektor pro připojení kabelů napájení. 


![Zapojení](/Images/wiring_pager.png)
![Realizace](/Images/Pager.jpg)
## Dokumentace

[NodeMCU dokumentace](https://nodemcu.readthedocs.io/en/master/)


[Bootstrap dokumentace](https://getbootstrap.com/docs/4.0/getting-started/introduction/)



